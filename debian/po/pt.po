# Translation of mopidy's debconf messages to Portuguese
# Copyright (C) 2014 THE mopidy'S COPYRIGHT HOLDER
# This file is distributed under the same license as the mopidy package.
#
# Paulo Tomé <paulo.jorge.tome@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: mopidy 0.18.3-4\n"
"Report-Msgid-Bugs-To: mopidy@packages.debian.org\n"
"POT-Creation-Date: 2014-08-05 07:37+0200\n"
"PO-Revision-Date: 2014-08-18 02:14+0100\n"
"Last-Translator: Paulo Tomé <paulo.jorge.tome@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.4\n"

#. Type: boolean
#. Description
#: ../mopidy.templates:2001
msgid "Start the Mopidy server at boot?"
msgstr "Iniciar o servidor Mopidy no arranque?"

#. Type: boolean
#. Description
#: ../mopidy.templates:2001
msgid ""
"The Mopidy server can be run as a system service, automatically starting at "
"boot. It will be listening to MPD connections on port 6600 and HTTP "
"connections on port 6680. By default, it will only accept connections from "
"the local machine."
msgstr ""
"O servidor Mopidy poderá correr como um serviço do sistema, inicializando-se "
"automaticamente no arranque. Irá ficar à escuta de ligações MPD no porto "
"6600 e de ligações HTTP no porto 6680. Por predefinição, apenas aceitará "
"ligações da máquina local."

#. Type: boolean
#. Description
#: ../mopidy.templates:2001
msgid ""
"You have the option of starting the Mopidy server automatically on system "
"boot. If in doubt, it is suggested to not start it automatically on boot."
msgstr ""
"Terá a opção de iniciar automaticamente o servidor Mopidy no arranque do "
"sistema. Em caso de dúvida, sugere-se que não o inicie automaticamente no "
"arranque."

#. Type: boolean
#. Description
#: ../mopidy.templates:2001
msgid ""
"This setting can be modified later by running \"dpkg-reconfigure mopidy\"."
msgstr ""
"Esta configuração poderá ser modificada mais tarde executando \"dpkg-"
"reconfigure mopidy\"."
