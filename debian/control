Source: mopidy
Section: sound
Priority: optional
Maintainer: Stein Magnus Jodal <jodal@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               dh-sequence-movetousr,
               po-debconf,
               python3,
               python3-pygraphviz,
               python3-pykka,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://mopidy.com/
Vcs-Git: https://salsa.debian.org/mopidy-team/mopidy.git
Vcs-Browser: https://salsa.debian.org/mopidy-team/mopidy

Package: mopidy
Architecture: all
Pre-Depends: ${misc:Pre-Depends},
Depends: adduser,
         debconf,
         gir1.2-gst-plugins-base-1.0,
         gir1.2-gstreamer-1.0,
         gstreamer1.0-plugins-good,
         gstreamer1.0-plugins-ugly,
         python3-gst-1.0,
         python3-tornado (>= 4.4),
         ${misc:Depends},
         ${python3:Depends},
Recommends: gstreamer1.0-alsa,
            gstreamer1.0-pulseaudio,
            gstreamer1.0-tools,
Suggests: mopidy-doc,
Description: extensible music server
 Mopidy plays music from local disk, Spotify, SoundCloud, Google Play Music,
 and more. You can edit the playlist from any phone, tablet, or computer using
 a variety of MPD and web clients.
 .
 Vanilla Mopidy only plays music from files and radio streams. Through
 extensions, Mopidy can play music from cloud services like Spotify,
 SoundCloud, and Google Play Music.

Package: mopidy-doc
Section: doc
Architecture: all
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
Recommends: mopidy,
Replaces: mopidy (<< 0.7.3-2),
Built-Using: ${sphinxdoc:Built-Using},
Description: extensible music server - documentation
 Mopidy plays music from local disk, Spotify, SoundCloud, Google Play Music,
 and more. You can edit the playlist from any phone, tablet, or computer using
 a variety of MPD and web clients.
 .
 This package provides the documentation for Mopidy.
